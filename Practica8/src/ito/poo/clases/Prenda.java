package ito.poo.clases;
import java.util.ArrayList;
public class Prenda {
	private int Modelo;
	private String Tela;	
	private float CostoProduccion;
	private String Genero;
	private String Temporada;
	private ArrayList<Lote> Lotes;
	
	public Prenda(int modelo, String tela, float costoProduccion, String genero, String temporada) {
		super();
		this.Modelo = modelo;
		Tela = tela;
		CostoProduccion = costoProduccion;
		Genero = genero;
		Temporada = temporada;
		Lotes = new ArrayList<Lote>();
	}

	public void AddNvoLote(Lote lote) {
		Lotes.add(lote);
}
	public Lote RecuperarLote(int i) {
		Lote l=null;
		if(Lotes.size()>i)
			l=Lotes.get(i);
		return l;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(CostoProduccion);
		result = prime * result + ((Genero == null) ? 0 : Genero.hashCode());
		result = prime * result + ((Lotes == null) ? 0 : Lotes.hashCode());
		result = prime * result + Modelo;
		result = prime * result + ((Tela == null) ? 0 : Tela.hashCode());
		result = prime * result + ((Temporada == null) ? 0 : Temporada.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prenda other = (Prenda) obj;
		if (Float.floatToIntBits(CostoProduccion) != Float.floatToIntBits(other.CostoProduccion))
			return false;
		if (Genero == null) {
			if (other.Genero != null)
				return false;
		} else if (!Genero.equals(other.Genero))
			return false;
		if (Lotes == null) {
			if (other.Lotes != null)
				return false;
		} else if (!Lotes.equals(other.Lotes))
			return false;
		if (Modelo != other.Modelo)
			return false;
		if (Tela == null) {
			if (other.Tela != null)
				return false;
		} else if (!Tela.equals(other.Tela))
			return false;
		if (Temporada == null) {
			if (other.Temporada != null)
				return false;
		} else if (!Temporada.equals(other.Temporada))
			return false;
		return true;
	}
	public int getModelo() {
		return Modelo;
	}
	public void setModelo(int modelo) {
		Modelo = modelo;
	}
	public String getTela() {
		return Tela;
	}
	public void setTela(String tela) {
		Tela = tela;
	}
	public float getCostoProduccion() {
		return CostoProduccion;
	}
	public void setCostoProduccion(float costoProduccion) {
		CostoProduccion = costoProduccion;
	}
	public String getGenero() {
		return Genero;
	}
	public void setGenero(String genero) {
		Genero = genero;
	}
	public String getTemporada() {
		return Temporada;
	}
	
	public int compareTo(Prenda arg0) {
	int r=0;
	if (this.Modelo!=arg0.getModelo())
		return  this.Modelo>arg0.getModelo()? 1:-1;
	else if (!this.Genero.equals(arg0.getGenero()))
		return this.Genero.compareTo(arg0.getGenero());
	else if (!this.Temporada.equals(arg0.getTemporada()))
		return this.Temporada.compareTo(arg0.getTemporada());
	else if (!this.Tela.equals(arg0.getTela()))
		return this.Tela.compareTo(arg0.getTela());
	return r;
	}
	@Override
	public String toString() {
		return "Prenda [Modelo=" + Modelo + ", Tela=" + Tela + ", CostoProduccion=" + CostoProduccion + ", Genero="
				+ Genero + ", Temporada=" + Temporada + ", Lotes=" + Lotes + "]";
	}
}