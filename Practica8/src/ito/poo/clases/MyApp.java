package ito.poo.clases;
import java.time.LocalDate;
import ito.poo.clases.Lote;
import ito.poo.clases.Prenda;

public class MyApp {
	public static void main(String[] args) {
		Prenda r1=new Prenda(26, "Algodon", 274.49f, "Mixto", "Otroo");
		Prenda r2=new Prenda(26, "Algodon", 274.49f, "Mixto", "Otroo");
	    System.out.println(r1);
	    System.out.println(r2);
	    
	    System.out.println(!r1.equals(r2));
	    System.out.println(r2.compareTo(r1));

	    Lote c1=new Lote(23, 135, LocalDate.of(2021, 02, 20));
	    Lote c2=new Lote(23, 135, LocalDate.of(2021, 02, 20));
	    System.out.println(c1);
	    System.out.println(c2);
	    
	    System.out.println(!c1.equals(c2));
	    System.out.println(c2.compareTo(c1));
	}
}