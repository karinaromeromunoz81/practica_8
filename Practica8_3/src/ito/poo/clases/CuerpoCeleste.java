package ito.poo.clases;
import java.util.ArrayList;
public class CuerpoCeleste {
	
	private String Nombre;
	private ArrayList<Ubicacion> Localizaciones;
	private String Composicion;
	
	public CuerpoCeleste(String nombre, String composicion) {
		super();
		Nombre = nombre;
		Composicion = composicion;
		Localizaciones = new ArrayList<Ubicacion>();
	}
	public float Desplazamiento(int i, int j) {
		float desplazamiento=-1;
		int t=Localizaciones.size();
		if(i<t && j<t) {
			float di=Localizaciones.get(i).getDistancia();
			float dj=Localizaciones.get(j).getDistancia();
			desplazamiento=Math.abs(di-dj);
		}
		return desplazamiento;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public ArrayList<Ubicacion> getLocalizaciones() {
		return Localizaciones;
	}
	public void agregaUbicacion(Ubicacion u) {
		this.Localizaciones.add(u);
	}
	public String getComposicion() {
		return Composicion;
	}
	public void setComposicion(String composicion) {
		Composicion = composicion;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Composicion == null) ? 0 : Composicion.hashCode());
		result = prime * result + ((Localizaciones == null) ? 0 : Localizaciones.hashCode());
		result = prime * result + ((Nombre == null) ? 0 : Nombre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuerpoCeleste other = (CuerpoCeleste) obj;
		if (Composicion == null) {
			if (other.Composicion != null)
				return false;
		} else if (!Composicion.equals(other.Composicion))
			return false;
		if (Localizaciones == null) {
			if (other.Localizaciones != null)
				return false;
		} else if (!Localizaciones.equals(other.Localizaciones))
			return false;
		if (Nombre == null) {
			if (other.Nombre != null)
				return false;
		} else if (!Nombre.equals(other.Nombre))
			return false;
		return true;
	}
	public int compareTo(CuerpoCeleste arg0) {
		int r = 0;
		if (!this.Nombre.equals(arg0.getNombre()))
			return this.Nombre.compareTo(arg0.getNombre());
		else if (!this.Composicion.equals(arg0.getComposicion()))
			return this.Composicion.compareTo(arg0.getComposicion());
		return r;
	}
	@Override
	public String toString() {
		return "CuerpoCeleste [Nombre=" + Nombre + ", Localizaciones=" + Localizaciones + ", Composicion=" + Composicion
				+ "]";
	}
	
}