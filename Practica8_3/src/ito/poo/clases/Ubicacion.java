package ito.poo.clases;
public class Ubicacion {
	
	private float Longitud;
	private float Latitud;
	private String Periodo;
	private float Distancia;
	
	public Ubicacion(float longitud, float latitud, String periodo, float distancia) {
		super();
		Longitud = longitud;
		Latitud = latitud;
		Periodo = periodo;
		Distancia = distancia;
	}
	public float getLongitud() {
		return Longitud;
	}
	public void setLongitud(float longitud) {
		Longitud = longitud;
	}
	public float getLatitud() {
		return Latitud;
	}
	public void setLatitud(float latitud) {
		Latitud = latitud;
	}
	public float getDistancia() {
		return Distancia;
	}
	public void setDistancia(float distancia) {
		Distancia = distancia;
	}
	public String getPeriodo() {
		return Periodo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(Distancia);
		result = prime * result + Float.floatToIntBits(Latitud);
		result = prime * result + Float.floatToIntBits(Longitud);
		result = prime * result + ((Periodo == null) ? 0 : Periodo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ubicacion other = (Ubicacion) obj;
		if (Float.floatToIntBits(Distancia) != Float.floatToIntBits(other.Distancia))
			return false;
		if (Float.floatToIntBits(Latitud) != Float.floatToIntBits(other.Latitud))
			return false;
		if (Float.floatToIntBits(Longitud) != Float.floatToIntBits(other.Longitud))
			return false;
		if (Periodo == null) {
			if (other.Periodo != null)
				return false;
		} else if (!Periodo.equals(other.Periodo))
			return false;
		return true;
	}
	public int compareTo(Ubicacion arg0) {
		int r = 0;
		if (!this.Periodo.equals(arg0.getPeriodo()))
			return this.Periodo.compareTo(arg0.getPeriodo());
		else if (this.Distancia != arg0.getDistancia())
			return this.Distancia > arg0.getDistancia() ? 1 : -1;
		else if (this.Latitud != arg0.getLatitud())
			return this.Latitud > arg0.getLatitud() ? 2 : -2;
		else if (this.Longitud != arg0.getLongitud())
			return this.Longitud > arg0.getLongitud() ? 3 : -3;
		return r;
	}
	@Override
	public String toString() {
		return "Ubicacion [Longitud=" + Longitud + ", Latitud=" + Latitud + ", Periodo=" + Periodo + ", Distancia="
				+ Distancia + "]";
	}
}