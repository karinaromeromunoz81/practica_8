package ito.poo.clases;
import java.util.ArrayList;
public class Fruta {
	
	private String Nombre;
	private float Extencion;
	private float CostoPromedio;
	private float PrecioVentaProm;
	private ArrayList<Periodo> Periodos;
	
	public Fruta(String nombre, float extencion, float costoPromedio, float precioVentaProm) {
		super();
		Nombre = nombre;
		Extencion = extencion;
		CostoPromedio = costoPromedio;
		PrecioVentaProm = precioVentaProm;
		this.Periodos=new ArrayList<Periodo>();
	}
	public void AgregarPeriodo(String tiempo, float cantidad) {
		Periodo p=new Periodo(tiempo,cantidad);
		Periodos.add(p);
	}

	public boolean EliminarPeriodo(int i) {
		boolean test=false;
		if(Periodos.size()>i);
		test =true;
		return test;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public float getExtencion() {
		return Extencion;
	}

	public void setExtencion(float extencion) {
		Extencion = extencion;
	}

	public float getCostoPromedio() {
		return CostoPromedio;
	}

	public void setCostoPromedio(float costoPromedio) {
		CostoPromedio = costoPromedio;
	}

	public float getPrecioVentaProm() {
		return PrecioVentaProm;
	}
	
	public float CostoPeriodo(int i) {
		float costo =0;
		if(this.Periodos.size()> i) {
			costo=this.CostoPromedio*this.Periodos.get(i).getCantCosechaXTiempo();
			return costo;
		}
		return costo;
	}
	
	public float GananciaEstimada(int i) {
		float ganancias =0;
		if(this.Periodos.size()> i) {
			float ventas =this.PrecioVentaProm*this.Periodos.get(i).getCantCosechaXTiempo();
			float costos = CostoPeriodo(i);
			ganancias =ventas -costos;
		return ganancias ;
	}
		return ganancias;
}

	public void setPrecioVentaProm(float precioVentaProm) {
		PrecioVentaProm = precioVentaProm;
	}

	public ArrayList<Periodo> getPeriodos() {
		return Periodos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(CostoPromedio);
		result = prime * result + Float.floatToIntBits(Extencion);
		result = prime * result + ((Nombre == null) ? 0 : Nombre.hashCode());
		result = prime * result + ((Periodos == null) ? 0 : Periodos.hashCode());
		result = prime * result + Float.floatToIntBits(PrecioVentaProm);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fruta other = (Fruta) obj;
		if (Float.floatToIntBits(CostoPromedio) != Float.floatToIntBits(other.CostoPromedio))
			return false;
		if (Float.floatToIntBits(Extencion) != Float.floatToIntBits(other.Extencion))
			return false;
		if (Nombre == null) {
			if (other.Nombre != null)
				return false;
		} else if (!Nombre.equals(other.Nombre))
			return false;
		if (Periodos == null) {
			if (other.Periodos != null)
				return false;
		} else if (!Periodos.equals(other.Periodos))
			return false;
		if (Float.floatToIntBits(PrecioVentaProm) != Float.floatToIntBits(other.PrecioVentaProm))
			return false;
		return true;
	}
	public int compareTo(Fruta arg0) {
		int r=0;
		if (!this.Nombre.equals(arg0.getNombre()))
			return this.Nombre.compareTo(arg0.getNombre());
		else if (this.Extencion != arg0.getExtencion())
			return this.Extencion>arg0.getExtencion()?1:-1;
		else if (this.PrecioVentaProm != arg0.getPrecioVentaProm())
			return this.PrecioVentaProm>arg0.getPrecioVentaProm()?2:-2;
		else if (this.CostoPromedio != arg0.getCostoPromedio())
			return this.CostoPromedio>arg0.getCostoPromedio()?3:-3;
		return r;
	}
	@Override
	public String toString() {
		return "Fruta [Nombre=" + Nombre + ", Extencion=" + Extencion + ", CostoPromedio=" + CostoPromedio
				+ ", PrecioVentaProm=" + PrecioVentaProm + ", Periodos=" + Periodos + "]";
	}
}