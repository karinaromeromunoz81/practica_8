package ito.poo.clases;
public class Periodo {
	
	private String TiempoCosecha;
	private float CantCosechaXTiempo;
	
	public Periodo(String tiempoCosecha, float cantCosechaXTiempo) {
		super();
		TiempoCosecha = tiempoCosecha;
		CantCosechaXTiempo = cantCosechaXTiempo;
	}
	public float CostoPeriodo() {
		return 0;
	}
	public float GananciaEstimada() {
		return 0;
	}
	
	public String getTiempoCosecha() {
		return TiempoCosecha;
	}
	public float getCantCosechaXTiempo() {
		return CantCosechaXTiempo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(CantCosechaXTiempo);
		result = prime * result + ((TiempoCosecha == null) ? 0 : TiempoCosecha.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Periodo other = (Periodo) obj;
		if (Float.floatToIntBits(CantCosechaXTiempo) != Float.floatToIntBits(other.CantCosechaXTiempo))
			return false;
		if (TiempoCosecha == null) {
			if (other.TiempoCosecha != null)
				return false;
		} else if (!TiempoCosecha.equals(other.TiempoCosecha))
			return false;
		return true;
	}
	public int compareTo(Periodo arg0) {
		int r=0;
		if (!this.TiempoCosecha.equals(arg0.getTiempoCosecha()))
				return this.TiempoCosecha.compareTo(arg0.getTiempoCosecha());
		else if (this.CantCosechaXTiempo != arg0.getCantCosechaXTiempo())
			return this.CantCosechaXTiempo>arg0.getCantCosechaXTiempo()?1:-1;
		return r;
	}
	@Override
	public String toString() {
		return "Periodo [TiempoCosecha=" + TiempoCosecha + ", CantCosechaXTiempo=" + CantCosechaXTiempo + "]";
	}
}